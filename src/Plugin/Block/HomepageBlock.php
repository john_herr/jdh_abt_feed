<?php
/**
 * @file
 * Block for the John Herr / ABT Feed module
 */

namespace Drupal\jdh_abt_feed\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;

/**
 * Provides a block of RSS feeds
 * @Block(
 *   id = "jdh_abt_feed_homepage_block",
 *   admin_label = @Translation("John Herr / Atlantic BT Homepage block"),
 * )
 */
class HomepageBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array (
      '#type' => 'markup',
      '#markup' => $this->blockContent(),
      '#cache' => [ 'max-age' => 60 * 60 * 3, ], // 60 seconds * 60 minutes * 3 hours
      // '#cache' => [ 'max-age' => 0, ], // development code 
      '#attributes' => [ 'class' => ['jdh-abt-feed-hp-block'] ],
      '#attached' => array(
        'library' =>  array(
          'jdh_abt_feed/homepage-block'
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   * 
   * Get the content from a feed, and return usable HTML for the block content
   */
  public function blockContent() {

    // initial values
    $html = '';
    $item_string = '';

    // settings page integration
    $config = \Drupal::config('jdh_abt_feed.settings');
    $url = $config->get('jdh_abt_feed.url') ? $config->get('jdh_abt_feed.url') : 'https://www.atlanticbt.com/feed/';
    $limit = $config->get('jdh_abt_feed.limit') ? $config->get('jdh_abt_feed.limit') : 5;

    if( filter_var($url, FILTER_VALIDATE_URL) === FALSE ) {
      $message = 'Not a Valid Feed URL: ' . $url;
      \Drupal::logger('jdh_abt_feed')->error($message);
      return '';
    }

    // get a feed, and build an html string for a number of items
    try {

      // get the feed
      $client = \Drupal::httpClient();
      $response = $client->get($url);
      $data = (string) $response->getBody();
      $xml = simplexml_load_string($data);
      $all_items = array();

      // do we have at least one item in the feed?
      if( isset($xml->channel->item[0]) ) {
        foreach ($xml->channel->item as $item) {        
          $title = (string) $item->title[0];
          $link = (string) $item->link[0];
          $all_items[] = array($title,$link);
        }
        // enforce our limit, and build our string
        for( $i = 0; $i < $limit; $i++ ) {
          $title = $all_items[$i][0];
          $link = $all_items[$i][1];
          // ensure that title and link aren't blank
          if( $title && $link && filter_var($link, FILTER_VALIDATE_URL) !== FALSE ) {
            $item_string .= '<li><a target="_blank" href="' . $link . '">' . $title . '</a>';
          }
        }
      }

      // only build our parent <ul> in the html string if we have at least one valid result
      if( !empty($item_string) ) {
        $html = '<ul>' . $item_string . '</ul>';
      }
    }
    // $statuscode = $response->getStatusCode();
    catch (\GuzzleHttp\Exception\BadResponseException $e) {
      $message = 'Bad Response Requesting Feed: ' . $e->getResponse()->getBody()->getContents();
      \Drupal::logger('jdh_abt_feed')->error($message);
    }
    catch (Exception $e) {
      \Drupal::logger('jdh_abt_feed')->error($e->getMessage);
      return '';
    }

    // return html string, or empty string if no valid items were found
    return $html;
  }
}
