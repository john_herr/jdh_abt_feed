<?php
/**
 * @file
 * Settings Form for the John Herr / ABT Feed module
 */

namespace Drupal\jdh_abt_feed\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Settings form for John Herr / ABT Feed module 
 */
class SettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jdh_abt_feed_config_form'; 
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'jdh_abt_feed.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('jdh_abt_feed.settings');
    $form['url'] = array(
      '#type' => 'textfield', 
      '#title' => $this->t('Feed URL'), 
      '#default_value' => $config->get('jdh_abt_feed.url') ? $config->get('jdh_abt_feed.url') : 'https://www.atlanticbt.com/feed/', 
      '#required' => TRUE,
    );
    $form['limit'] = array(
      '#type' => 'textfield', 
      '#title' => $this->t('Feed Item Display Limit'), 
      '#default_value' => $config->get('jdh_abt_feed.limit') ? $config->get('jdh_abt_feed.limit') : 5, 
      '#required' => TRUE,
    );
    return $form; 
  }
 
  /**
   * {@inheritdoc}
   */ 
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('jdh_abt_feed.settings');
    $config->set('jdh_abt_feed.url', $form_state->getValue('url'));
    $config->set('jdh_abt_feed.limit', $form_state->getValue('limit'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

}
