# Installing the “John Herr Feed Test for Atlantic BT”

This module will display a configurable number of RSS items from a user-configurable RSS feed (specified by URL) so long as that feed is compatible with the RSS feed at https://www.atlanticbt.com/feed/, as of 1/11/2017.

- Download source code, rename folder to "jdh_abt_feed" and place that folder in your /modules directory.
- To install this module, go to your sites Extend page, at "/admin/modules".
- Click the checkbox next to "John Herr Feed Test for Atlantic BT"
- Click the Install button at the bottom of the page.
- The RSS field block should be automatically displayed in the sidebar_first region of the homepage.
- Caching is in place, and should be cleared when you alter the RSS settings at "admin/config/jdh_abt_feed/settings".